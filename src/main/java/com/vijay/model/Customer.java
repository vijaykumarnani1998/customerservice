package com.vijay.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="Customer_Table")
public class Customer {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="Cust_Id_COl")
	private Long id;
	@Column(name="Cust_Name_COl")
	private String name;
	@Column(name="Cust_Email_COl")
	private String email;
	@Column(name="Cust_Gender_COl")
	private String gender;
	@Column(name="Cust_Image_COl")
	private String imagePath;
	@Column(name="Cust_Mobile_COl")
	private String mobile;
	@Column(name="Cust_Addr_COl")
	private String address;
	@Column(name="Cust_PanCard_COl")
	private String panCardId;
	@Column(name="Cust_Aadhar_COl")
	private String aadharId;


}
