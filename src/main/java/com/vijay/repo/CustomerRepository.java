package com.vijay.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vijay.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
	
	 //@Query("SELECT email FROM Customer where email =:email")
	Optional<Customer>  findByEmail(String email);
	
	 //@Query("SELECT mobile FROM Customer where mobile =:mobile")
	Optional<Customer>  findByMobile(String mobile);
	
	 // @Query("SELECT panCardId FROM Customer where panCardId =:panCardId")
	Optional<Customer>  findByPanCardId(String panCardId);
	
	 //@Query("SELECT aadharId FROM Customer where aadharId =:aadharId")
	Optional<Customer>  findByAadharId(String aadharId);

}
