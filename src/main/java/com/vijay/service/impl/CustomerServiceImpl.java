package com.vijay.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vijay.exception.CustomerNotFoundException;
import com.vijay.model.Customer;
import com.vijay.repo.CustomerRepository;
import com.vijay.service.CustomerService;
@Service
public class CustomerServiceImpl implements CustomerService {
    
	@Autowired
	 private CustomerRepository repo;
	@Override
	public Long createCustomer(Customer customer) {
		return repo.save(customer).getId() ;
	}

	@Override
	public List<Customer> getAllCustomers() {
		return repo.findAll();
	}

	@Override
	public Customer findById(Long id) {
		Customer customer=repo.findById(id).orElseThrow(
				()->new CustomerNotFoundException("Customer  '"+id+"' Not Exists"));
		return customer;
	}

	@Override
	public Customer findByEmail(String email) {
		Customer customer= repo.findByEmail(email).orElseThrow(
				()->new CustomerNotFoundException("Customer  '"+email+"' Not Exists"));
		return customer;

	}
	

	@Override
	public Customer findByMobile(String mobile) {
		 Customer customer=repo.findByMobile(mobile).orElseThrow(
					()->new CustomerNotFoundException("Customer  '"+mobile+"' Not Exists"));
		return customer;
	}

	@Override
	public Customer findByPanCard(String pancardId) {
		 Customer customer=repo.findByPanCardId(pancardId).orElseThrow(
					()->new CustomerNotFoundException("Customer  '"+pancardId+"' Not Exists"));
		return customer;
	}

	@Override
	public Customer findByAadhar(String aadharId) {
		 Customer customer=repo.findByAadharId(aadharId).orElseThrow(
					()->new CustomerNotFoundException("Customer  '"+aadharId+"' Not Exists"));
		return customer;
}
}
