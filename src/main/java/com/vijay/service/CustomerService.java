package com.vijay.service;

import java.util.List;

import com.vijay.model.Customer;

public interface CustomerService {
	
	public  Long createCustomer(Customer customer);
	
	public  List<Customer> getAllCustomers();
	
	public  Customer findById(Long id );
	public  Customer findByEmail(String email );
	public  Customer findByMobile(String  mobile);
	public  Customer findByPanCard(String pancardId);
	public  Customer findByAadhar(String aadharId);
   
}
