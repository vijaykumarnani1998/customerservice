package com.vijay.exception;

public class CustomerNotFoundException  extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9048929411917274938L;
  
	 public CustomerNotFoundException()
	 {
		 super();
		 
	 }
	 public CustomerNotFoundException(String message)
	 {
		 super(message);
		 
	 }
	
	
}
