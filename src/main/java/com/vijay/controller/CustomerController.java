package com.vijay.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vijay.model.Customer;
import com.vijay.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {
	
	@Autowired
	private CustomerService service;
	
	
	@PostMapping("/create")
	public ResponseEntity<String> createCustomer(@RequestBody Customer customer)
	{
	Long id=	service.createCustomer(customer);
	String message =("Customer Created with Id:" +id);
	return new ResponseEntity<String> (message,HttpStatus.CREATED);
	}
	
	
	@GetMapping("fetch/all")
	public ResponseEntity<List<Customer>> getAllCustomers()
	{
		List<Customer> list=service.getAllCustomers();
		return new ResponseEntity<List<Customer>> (list,HttpStatus.CREATED);
	} 
	
	@GetMapping("fetchEmail/{email}")
	public ResponseEntity<Customer> getByEmail(@PathVariable String email)
	{
		Customer customer=service.findByEmail(email);
		return new ResponseEntity<Customer> (customer,HttpStatus.CREATED);
	}
	
	
	 @GetMapping("fetchMobile/{mobile}")
	
	public ResponseEntity<Customer> getByMobile(@PathVariable String mobile)
	{
		Customer customer=service.findByEmail(mobile);
		return new ResponseEntity<Customer> (customer,HttpStatus.CREATED);
	}
	
	@GetMapping("fetchPan/{panCardId}")
	public ResponseEntity<Customer> getBypanCardId(@PathVariable String panCardId)
	{
		Customer customer=service.findByPanCard(panCardId);
		return new ResponseEntity<Customer> (customer,HttpStatus.CREATED);
	}
	
	@GetMapping("fetchAadhar/{aadharId}")
	public ResponseEntity<Customer> getByAadhar(@PathVariable String aadharId)
	{
		Customer customer=service.findByAadhar(aadharId);
		return new ResponseEntity<Customer> (customer,HttpStatus.CREATED);
	}
 
}
